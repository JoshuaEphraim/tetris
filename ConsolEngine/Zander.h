
#pragma once

#include "Block.h"

class Zander : public Block
{
	typedef Block Parent;
	int forms[2][4][2] = {
		{ { 0 , 0 }, { 1 , 0 }, { 1 , 1 }, { 2, 1 } },
		{ { 1 , 0 }, { 1 , 1 }, { 0 , 1 }, { 0, 2 } }
	};
protected:
	int ** getCoordinates(int * position) override;
public:
	Zander(int startX, int startTurn);
	void turn() override;
	void retrieve() override;
};
