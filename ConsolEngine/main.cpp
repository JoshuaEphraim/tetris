// Copyright 2009-2014 Blam Games, Inc. All Rights Reserved.

#include <iostream>
#include "TetrisApp.h"
#include <time.h>

void main ()
{
	srand(time(0));
	TetrisApp app;
	app.Run();
}
