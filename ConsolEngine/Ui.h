#include <vector>
#include "Point.h"
#include <string>
using namespace std;

class Ui
{
	int previewPosition[2];
	int score;
	int width;
	string score_str = "> Score: ";
	vector<Point> frame;
	vector<Point> pole;
	vector<Point> bunch;
public:
	Ui(int x_size, int y_size);
	int getScore();
	vector<Point> getFrame();
	vector<Point> getPole();
	vector<Point> getBunch();
	int * getPreviewPosition();
	bool checkCollision(int ** block, int moveX = 0, int moveY = 0);
	void consume(int ** block);
	void clearFullLines();
	void moveDown(int line);
	string getScoreStr();
	vector<Point> sortBunchByY(vector<Point> bunch);
};
