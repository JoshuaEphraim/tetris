// Copyright 2009-2014 Blam Games, Inc. All Rights Reserved.
#include "Block.h"

Block::Block(int startX, int startTurn)
{
	position[0] = startX;
	position[1] = 1;
	side = startTurn;
}
void Block::moveX(int number)
{
	position[0] += number;
}
void Block::moveY(int number)
{
	position[1] += number;
}
int Block::getSide()
{
	return side;
}
int* Block::getPosition()
{
	return position;
}
void Block::setSide(int new_turn)
{
	side = new_turn;
}
int** Block::getCoordinates(int * position)
{
	int **coordinates = new int*[4];
	coordinates[0] = new int[2];
	coordinates[0][0] = forms[0][0] + position[0];
	coordinates[0][1] = forms[0][1] + position[1];
	return coordinates;
}
int** Block::getPositionCoords()
{
	int * position = getPosition();
	return getCoordinates(position);
}
int** Block::getPreviewCoords(int * position)
{
	return getCoordinates(position);
}
void Block::turn()
{
	setSide(0);
}
void Block::retrieve()
{
	setSide(0);
}
