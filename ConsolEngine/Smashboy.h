
#pragma once

#include "Block.h"

class Smashboy : public Block
{
	typedef Block Parent;
	int forms[4][2] ={ { 0 , 0 }, { 0 , 1 }, { 1 , 1 }, { 1, 0 } };
protected:
	int ** getCoordinates(int * position) override;
public:
	Smashboy(int startX, int startTurn);
};
