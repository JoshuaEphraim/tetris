// Copyright 2009-2014 Blam Games, Inc. All Rights Reserved.

#pragma once

#include "BaseApp.h"
#include "Point.h"
#include "Lan.h"
#include "Justin.h"
#include "Lucy.h"
#include "Smashboy.h"
#include "Tina.h"
#include "RhodeIslandZ.h"
#include "Zander.h"
#include "Ui.h"

class TetrisApp : public BaseApp
{
	typedef BaseApp Parent;

private:
	bool need_new = true;
	bool fast = false;
	int move = 5;
	Block *block;
	Block *next_block;
	Ui *ui;

public:
	TetrisApp();
	void KeyPressed(int btnCode);
	void UpdateF(float deltaTime);
	void PaintUi();
	void PaintBlock();
	void CreateBlock();
	void NextStep();
	void showNext();
	void hideNext();
	void showScore(string score);
	Block* getRandomBlock();
};
