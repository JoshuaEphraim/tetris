// Copyright 2009-2014 Blam Games, Inc. All Rights Reserved.

#include "TetrisApp.h"
#include <cstdlib>

TetrisApp::TetrisApp() : Parent(26, 26)
{
	ui = new Ui( 26, 26);
	next_block = getRandomBlock();
	CreateBlock();
}

void TetrisApp::KeyPressed(int btnCode)
{
	if (btnCode == 75) {
		if (!ui->checkCollision(block->getPositionCoords(), -1))
			block->moveX(-1);
	}
	else if (btnCode == 77) {
		if (!ui->checkCollision(block->getPositionCoords(), 1))
			block->moveX(1);
	}
	else if (btnCode == 80) {
		fast = true;
	}
	else if (btnCode == 32) {
		block->turn();
		if (ui->checkCollision(block->getPositionCoords(), 0, 0))
			block->retrieve();
	}
}

void TetrisApp::UpdateF(float deltaTime)
{
	PaintUi();
	PaintBlock();
	if(fast || move%7==0)
		NextStep();
	Sleep(35);
	move++;
}

void TetrisApp::PaintUi()
{
	vector<Point> frame = ui->getFrame();
	std::vector<Point>::iterator iter = frame.begin();
	while (iter != frame.end())
	{
		Point point = *iter;
		SetChar(point.x, point.y, '#');
		++iter;
	}
	frame = ui->getPole();
	iter = frame.begin();
	while (iter != frame.end())
	{
		Point point = *iter;
		SetChar(point.x, point.y, '-');
		++iter;
	}
	frame = ui->getBunch();
	iter = frame.begin();
	while (iter != frame.end())
	{
		Point point = *iter;
		SetChar(point.x, point.y, '*');
		++iter;
	}
	showNext();
	showScore(ui->getScoreStr());
}

void TetrisApp::showScore(string score)
{
	int x = 3;
	int y = 24;
	for (char& c : score) {
		SetChar(x, y, c);
		++x;
	}
}

void TetrisApp::showNext()
{
	int ** coordinates = next_block->getPreviewCoords(ui->getPreviewPosition());
	for (int i = 0; i < 4; ++i) {
		SetChar(coordinates[i][0], coordinates[i][1], '*');
	}
}

void TetrisApp::hideNext()
{
	int ** coordinates = next_block->getPreviewCoords(ui->getPreviewPosition());
	for (int i = 0; i < 4; ++i) {
		SetChar(coordinates[i][0], coordinates[i][1], L' ');
	}
}

void TetrisApp::PaintBlock()
{
	int ** coordinates = block->getPositionCoords();
	for (int i = 0; i < 4; ++i) {
		SetChar(coordinates[i][0], coordinates[i][1], '*');
	}
}

void TetrisApp::CreateBlock()
{
	fast = false;
	block = next_block;
	hideNext();
	next_block = getRandomBlock();
	if(ui->checkCollision(block->getPositionCoords(), 0, 0))
		exit(0);
}

Block* TetrisApp::getRandomBlock()
{
	int start_turn = std::rand() % 3;
	int start_x = std::rand() % 13 + 1;
	int rand = std::rand() % 7;
	switch (rand) {
	case 0: return new Justin(start_x, start_turn);
	case 1: return new Lan(start_x, start_turn/2);
	case 2: return new Lucy(start_x, start_turn);
	case 3: return new Smashboy(start_x, start_turn);
	case 4: return new Tina(start_x, start_turn);
	case 5: return new RhodeIslandZ(start_x, start_turn/2);
	case 6: return new Zander(start_x, start_turn/2);
	}
}

void TetrisApp::NextStep()
{
	if (ui->checkCollision(block->getPositionCoords(), 0, 1)) {
		ui->consume(block->getPositionCoords());
		CreateBlock();
	}
	else {
		block->moveY(1);
	}
}
