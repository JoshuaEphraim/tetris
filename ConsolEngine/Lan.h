
#pragma once

#include "Block.h"

class Lan : public Block
{
	typedef Block Parent;
	private:
		int forms[2][4][2] = {
			{ { 0 , 0 }, { 0 , 1 }, { 0 , 2 }, { 0, 3 } },
			{ { 0 , 0 }, { 1 , 0 }, { 2 , 0 }, { 3, 0 } }
		};
	protected:
		int ** getCoordinates(int * position) override;
	public:
		Lan(int startX, int startTurn);
		void turn() override;
		void retrieve() override;
};
