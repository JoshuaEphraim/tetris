
#pragma once

class Block
{
	int position[2];
	int side = 0;
	int forms[1][2] = { { 0 , 0 } };
protected:
	int getSide();
	int* getPosition();
	void setSide(int new_turn);
	virtual int ** getCoordinates(int * position);
public:
	Block(int startX, int startTurn);
	void moveX(int number);
	void moveY(int number);
	virtual void turn();
	virtual void retrieve();
	virtual int ** getPositionCoords();
	virtual int ** getPreviewCoords(int * position);
};
