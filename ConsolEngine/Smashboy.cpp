#include "Smashboy.h"



Smashboy::Smashboy(int startX, int startTurn) : Parent(startX, startTurn)
{
}
int** Smashboy::getCoordinates(int * position)
{
	int **coordinates = new int*[4];
	for (int i = 0; i < 4; i++) {
		coordinates[i] = new int[2];
		coordinates[i][0] = forms[i][0] + position[0];
		coordinates[i][1] = forms[i][1] + position[1];
	}
	return coordinates;
}
