// Copyright 2009-2014 Blam Games, Inc. All Rights Reserved.

#include "Ui.h"
#include <map>

Ui::Ui(int x_size, int y_size)
{
	width = 15;
	score = 0;
	previewPosition[0] = 20;
	previewPosition[1] = 5;
	Point point;
	for (int y = 0; y <= y_size; y++)
		for (int x = 0; x <= x_size; x++) {
			point.x = x;
			point.y = y;
			if (x == 0 || y == 0 || x == x_size || y == y_size || (x == 16 && y < 22) || y == 22) {
				frame.push_back(point);
			}
			if (x < 16 && x > 0 && y > 0 && y < 22)
				pole.push_back(point);
		}
}
int Ui::getScore()
{
	return score;
}
std::vector<Point> Ui::getFrame()
{
	return frame;
}
std::vector<Point> Ui::getPole()
{
	return pole;
}
std::vector<Point> Ui::getBunch()
{
	return bunch;
}
bool Ui::checkCollision(int ** block, int moveX, int moveY)
{
	vector<Point>::iterator iter = frame.begin();
	int x;
	int y;
	Point point;
	while (iter != frame.end())
	{
		point = *iter;
		for (int i = 0; i < 4; ++i) {
			x = block[i][0] + moveX;
			y = block[i][1] + moveY;
			if (x == point.x && y == point.y)
				return true;
		}
		++iter;
	}
	if (!bunch.empty()) {
		iter = bunch.begin();
		while (iter != bunch.end())
		{
			point = *iter;
			for (int i = 0; i < 4; ++i) {
				x = block[i][0] + moveX;
				y = block[i][1] + moveY;
				if (x == point.x && y == point.y)
					return true;
			}
			++iter;
		}
	}
	return false;
}
void Ui::clearFullLines()
{
	Point point;
	int line_y;
	vector<Point>::iterator start;
	vector<Point>::iterator end;
	vector <vector<Point>::iterator> line;
	vector <vector<Point>::iterator> toDelete;
	vector<Point>::iterator iter = bunch.begin();
	vector <vector<Point>::iterator>::iterator iterDelete;
	int prevY = 0;
	int delets = 0;
	while (iter != bunch.end())
	{
		point = *iter;
		if (prevY != 0 && prevY != point.y) {
			line.clear();
		}
		line.push_back(iter);
		if (line.size() == width) {
			start = line[0];
			point = *start;
			line_y = point.y;
			end = line.back();
			iter = bunch.erase(start,end);
			iter = bunch.erase(iter);
			score++;
			moveDown(line_y);
			if (!bunch.size());
				break;
			line.clear();
		}
		else {
			++iter;
		}
		prevY = point.y;
	}
}
void Ui::moveDown(int line)
{
	Point point;
	vector<Point>::iterator iter = bunch.begin();
	while (iter != bunch.end())
	{
		point = *iter;
		if (point.y < line) {
			point.y++;
			*iter = point;
		}
		++iter;
	}
}
vector<Point> Ui::sortBunchByY(vector<Point> sortedBunch)
{
	Point point;
	vector<Point> less;
	vector<Point> higher;
	Point check = sortedBunch.back();
	sortedBunch.pop_back();
	vector<Point>::iterator iter = sortedBunch.begin();
	while (iter != sortedBunch.end())
	{
		point = *iter;
		if(check.y > point.y)
			less.push_back(point);
		else
			higher.push_back(point);
		++iter;
	}
	if(less.size())
		less = sortBunchByY(less);
	if (higher.size())
		higher = sortBunchByY(higher);
	less.push_back(check);
	less.insert(less.end(), higher.begin(), higher.end());
	return less;
}
void Ui::consume(int ** block)
{
	Point point;
	for (int i = 0; i < 4; ++i) {
		point.x = block[i][0];
		point.y = block[i][1];
		bunch.push_back(point);
	}
	bunch = sortBunchByY(bunch);
	clearFullLines();
}
string Ui::getScoreStr()
{
	return score_str + to_string(score);
}
int * Ui::getPreviewPosition()
{
	return previewPosition;
}

