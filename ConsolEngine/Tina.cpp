#include "Tina.h"



Tina::Tina(int startX, int startTurn) : Parent(startX, startTurn)
{
}
int** Tina::getCoordinates(int * position)
{
	int **coordinates = new int*[4];
	int turn = getSide();
	for (int i = 0; i < 4; i++) {
		coordinates[i] = new int[2];
		coordinates[i][0] = forms[turn][i][0] + position[0];
		coordinates[i][1] = forms[turn][i][1] + position[1];
	}
	return coordinates;
}
void Tina::turn()
{
	int turn = getSide();
	if (turn < 3)
		setSide(++turn);
	else
		setSide(0);
}
void Tina::retrieve()
{
	int turn = getSide();
	if (turn == 0)
		setSide(3);
	else
		setSide(--turn);
}
