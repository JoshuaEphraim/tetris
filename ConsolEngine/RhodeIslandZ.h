
#pragma once

#include "Block.h"

class RhodeIslandZ : public Block
{
	typedef Block Parent;
	int forms[2][4][2] = {
		{ { 0 , 0 }, { 0 , 1 }, { 1 , 1 }, { 1, 2 } },
		{ { 0 , 1 }, { 1 , 1 }, { 1 , 0 }, { 2, 0 } }
	};
protected:
	int ** getCoordinates(int * position) override;
public:
	RhodeIslandZ(int startX, int startTurn);
	void turn() override;
	void retrieve() override;
};
