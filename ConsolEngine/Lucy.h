
#pragma once

#include "Block.h"

class Lucy : public Block
{
	typedef Block Parent;
	int forms[4][4][2] = {
		{ { 0 , 1 }, { 1 , 1 }, { 2 , 1 }, { 2, 0 } },
		{ { 0 , 0 }, { 1 , 0 }, { 1 , 1 }, { 1, 2 } },
		{ { 0 , 1 }, { 0 , 0 }, { 1 , 0 }, { 2, 0 } },
		{ { 0 , 0 }, { 0 , 1 }, { 0 , 2 }, { 1, 2 } }
	};
protected:
	int ** getCoordinates(int * position) override;
public:
	Lucy(int startX, int startTurn);
	void turn() override;
	void retrieve() override;
};
